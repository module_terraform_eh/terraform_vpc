##################
#
# VPC variable definition
#
##################

# Input variable of Pomelo VPC

variable "create_vpc" {
  description = "Controls if VPC should be created (it affects almost all resources)"
  type        = bool
  default     = true
}
variable "vpc_cidr"{
  description = "CIDR for the VPC"
  type        = string
  default     = "10.0.0.0/16"
}

variable "instance_tenancy" {
  description = "A tenancy option for instances launched into the VPC"
  type        = string
  default     = "default"
}

variable "enable_dns_hostnames" {
  description = "Should be true to enable DNS hostnames in the Default VPC"
  type        = bool
  default     = false
}

variable "enable_dns_support" {
  description = "Should be true to enable DNS support in the Default VPC"
  type        = bool
  default     = true
}

variable "enable_classiclink" {
  description = "Should be true to enable ClassicLink for the VPC. Only valid in regions and accounts that support EC2 Classic."
  type        = bool
  default     = null
}

variable "enable_classiclink_dns_support" {
  description = "Should be true to enable ClassicLink DNS Support for the VPC. Only valid in regions and accounts that support EC2 Classic."
  type        = bool
  default     = null
}

variable "enable_ipv6" {
  description = "Requests an Amazon-provided IPv6 CIDR block with a /56 prefix length for the VPC. You cannot specify the range of IP addresses, or the size of the CIDR block."
  type        = bool
  default     = false
}

variable "vpc_name"{
  description = "name of vpc"
  type        = string
  default     = ""
}

variable "vpc_creator"{
 description = "creator user name of resource"
 type        = string
 default     = "PRO"
}
variable "vpc_region"{
 description = "resource of region"
 type        = string
 default     = "us-east-1"
}
variable "vpc_env"{
 description = "environment for resource"
 type        = string
 default     = "dev"
}
variable "vpc_atomation"{
 type        = string
 default     = "terraform"
}
variable "account_id"{
 description = "account id for resource"
 type        = string
 default     = "007"
}
