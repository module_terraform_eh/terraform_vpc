##################
#
# VPC definition
#
##################

resource "aws_vpc" "Pomelo_VPC" {
  #count = var.create_vpc ? 1 : 0

  cidr_block                       = var.vpc_cidr
  instance_tenancy                 = var.instance_tenancy
  enable_dns_hostnames             = var.enable_dns_hostnames
  enable_dns_support               = var.enable_dns_support
  enable_classiclink               = var.enable_classiclink
  enable_classiclink_dns_support   = var.enable_classiclink_dns_support
  assign_generated_ipv6_cidr_block = var.enable_ipv6
  tags = {
    Name        = var.vpc_name
    creator     = var.vpc_creator
    region      = var.vpc_region
    environment = var.vpc_env
    atomation   = var.vpc_atomation
    account     = var.account_id
  }
}

output "vpc_id" {
  value = aws_vpc.Pomelo_VPC.id
  #value = aws_vpc.Pomelo_VPC[count.index].id
}
